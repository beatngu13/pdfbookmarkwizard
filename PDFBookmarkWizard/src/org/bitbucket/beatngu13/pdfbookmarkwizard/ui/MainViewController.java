/*
 * This file is part of the PDF Bookmark Wizard.
 * 
 * The PDF Bookmark Wizard is free software: you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License version 3 as 
 * published by the Free Software Foundation. <br><br>
 * 
 * The PDF Bookmark Wizard is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 * Public License for more details. <br><br>
 * 
 * You should have received a copy of the GNU General Public License along with 
 * the PDF Bookmark Wizard. If not, see 
 * <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>. <br><br>
 * 
 * Copyright 2013-2014 Daniel Kraus
 */
package org.bitbucket.beatngu13.pdfbookmarkwizard.ui;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bitbucket.beatngu13.pdfbookmarkwizard.core.Wizard;

import javafx.animation.FadeTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker.State;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.util.Duration;

/**
 * Provides a JavaFX-based Wizard UI.
 * 
 * @author danielkraus1986@gmail.com
 *
 */
public class MainViewController {
	
	/**
	 * {@link Logger} instance.
	 */
	private static final Logger logger = Logger.getLogger(MainViewController.class.getName());
	
	/**
	 * Sets {@link #root}.
	 */
	private DirectoryChooser directoryChooser = new DirectoryChooser();
	/**
	 * Sets {@link #root}.
	 */
	private FileChooser fileChooser = new FileChooser();
	/**
	 * Root directory or file to work with.
	 */
	private File root;
	/**
	 * Occurs when {@link #runButton} is being clicked.
	 */
	private WarningViewController warningController;
	/**
	 * Indicates whether multiple or single files will be processed.
	 */
	private boolean multipleMode = true;
	
	/**
	 * Wizard UI container.
	 */
	@FXML
	private Parent mainView;
	/**
	 * Switches between the processing of multiple or single files.
	 */
	@FXML
	private ToggleGroup modeToggleGroup;
	/**
	 * <code>Label</code> for {@link #rootTextField}. 
	 */
	@FXML
	private Label rootLabel;
	/**
	 * <code>TextField</code> for {@link #root}.
	 */
	@FXML
	private TextField rootTextField;
	/**
	 * Uses {@link #directoryChooser} respectively {@link #fileChooser} to set {@link #root}.
	 */
	@FXML
	private Button browseButton;
	/**
	 * <code>TextField</code> which is used for the copies infix.
	 */
	@FXML
	private TextField copyTextField;
	/**
	 * Enables the creation of copies.
	 */
	@FXML
	private CheckBox copyCheckBox;
	/**
	 * {@link VersionEnum} to use for modification.
	 */
	@FXML
	private ChoiceBox<String> zoomChoiceBox;
	/**
	 * {@link SerializationModeEnum} to use for modification.
	 */
	@FXML
	private ChoiceBox<String> versionChoiceBox;
	/**
	 * Displays the current state of the Wizard. Basically the values of {@link State} are used.
	 */
	@FXML
	private Text stateText;
	/**
	 * Calls {@link #run()} if the user confirms to proceed.
	 */
	@FXML
	private Button runButton;
	
	/**
	 * Creates a new <code>MainViewController</code> instance.
	 */
	public MainViewController() {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("MainView.fxml"));
			
			// TODO Adding the controller within the FXML file fails.
			loader.setController(this);
			loader.load();
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Could not load FXML file.", e);
		}
		
		runButton.disableProperty().bind(rootTextField.textProperty().isEqualTo("").or(
				stateText.textProperty().isEqualTo(State.RUNNING.toString())));
		copyTextField.disableProperty().bind(copyCheckBox.selectedProperty().not());

		directoryChooser.setTitle("Choose a directory");
		fileChooser.setTitle("Choose a file");
		
		// TODO Best practice?
		modeToggleGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {

			@Override
			public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue,
					Toggle newValue) {
				multipleMode = !rootLabel.getText().equals("Directory:");
				
				FadeTransition fadeOut = new FadeTransition(Duration.millis(300.0), rootLabel);
				fadeOut.setFromValue(1.0);
				fadeOut.setToValue(0.0);
				fadeOut.play();
				
				fadeOut.setOnFinished(new EventHandler<ActionEvent>() {
					
					@Override
					public void handle(ActionEvent event) {
						rootLabel.setText(multipleMode ? "Directory:" : "File:");
						
						FadeTransition fadeIn = new FadeTransition(Duration.millis(300.0), 
								rootLabel);
						fadeIn.setFromValue(0.0);
						fadeIn.setToValue(1.0);
						fadeIn.play();
					}
				});
			}
		});
		
		browseButton.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent arg0) {
				root = multipleMode ? directoryChooser.showDialog(mainView.getScene().getWindow()) 
						: fileChooser.showOpenDialog(mainView.getScene().getWindow());
				
				if (root != null) {
					File parentFile = root.getParentFile();
					
					rootTextField.setText(root.getAbsolutePath());
					directoryChooser.setInitialDirectory(parentFile);
					fileChooser.setInitialDirectory(parentFile);
				}
			}

		});
		
		runButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				if (validateInput()) {
					// TODO Best practice?
					warningController = warningController == null ? new WarningViewController(
							runButton.getScene().getWindow()) : warningController;
					
					String messagePrefix = multipleMode ? "All files in \"" + root.getAbsolutePath() 
							+ "\" and its enclosing subdirectories will be " 
							: "\"" + root.getAbsolutePath() + "\" will be ";
					String messageInfix = !copyCheckBox.isSelected() ? "overwritten!" 
							: "copied!";
					String messageSuffix = "\n\nAre you sure to proceed?";
					
					if (warningController.show(messagePrefix + messageInfix + messageSuffix)) {
						MainViewController.this.run();
					}
				}
			}
			
		});
	}
	
	/**
	 * Validates the given UI input.
	 * 
	 * @return <code>true</code> if everything is valid, else <code>false</code>.
	 */
	private boolean validateInput() {
		boolean valid = true;
		
		if (root == null
				|| !root.getAbsolutePath().equals(rootTextField.getText())) {
			root = new File(rootTextField.getText());
		}
		
		if (multipleMode && !root.isDirectory()) {
			valid = false;
			stateText.setText("A FILE IS SELECTED");
			logger.info("\"" + root.getAbsolutePath()
					+ "\" is a file.");
		} else if (!multipleMode && root.isDirectory()) {
			valid = false;
			stateText.setText("A DIRECTORY IS SELECTED");
			logger.info("\"" + root.getAbsolutePath()
					+ "\" is a directory.");
		}
		
		if (copyCheckBox.isSelected() && copyTextField.getText().isEmpty()) {
			valid = false;
			stateText.setText("FILENAME INFIX IS EMPTY");
			logger.info("Filename infix is empty.");
		}
		
		return valid;
	}
	
	/**
	 * Creates a new {@link Wizard} instance and starts modification on {@link #root}.
	 */
	private void run() {
		String filenameInfix = copyCheckBox.isSelected() ? copyTextField.getText() : null;
		Wizard wizard = new Wizard(root, filenameInfix, zoomChoiceBox.getValue(), 
				versionChoiceBox.getValue());
		Thread thread = new Thread(wizard);
		
		wizard.messageProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, 
					String newValue) {
				MainViewController.this.stateText.setText(newValue);
			}
		});
		
		thread.setDaemon(true);
		thread.start();
	}

	/**
	 * @return {@link #mainView}.
	 */
	public Parent getMainView() {
		return mainView;
	}
	
}
