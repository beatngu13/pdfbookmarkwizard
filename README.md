# ⚠️ WARNING ⚠️ #

The PDF Bookmark Wizard is *deprecated* and replaced by the [PDF Zoom Wizard](https://github.com/beatngu13/pdf-zoom-wizard/).

# PDF Bookmark Wizard #

The PDF Bookmark Wizard is a lightweight and cross-platform tool that allows you to easily apply zoom settings (such as the infamous "Inherit zoom") to a batch of PDF bookmarks.

If you are a user, read down below to check out the features of the Wizard and see how to get started. If you are a developer, please head on to the [wiki](https://bitbucket.org/beatngu13/pdfbookmarkwizard/wiki/) section.

This is free software under [GNU GPLv3](http://www.gnu.org/licenses/), so have a try on it. And if you like the tool, you can support its development by [donating](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=SYDFV6342B4T4) a few bucks.

## Features ##

- Apply "Fit page", "Actual size", "Fit width", "Fit visible" or "Inherit zoom" to your bookmarks without selecting them manually
- Choose between a single PDF file or whole directories (enclosing subdirectories included)
- Quickly overwrite your existing files or copy them with a custom filename infix
- Increase the version of your PDFs on the fly (*experimental*)

## Getting Started ##

Make sure you have [Java](http://java.com/en/download/index.jsp) installed on your computer. Afterwards [download](https://bitbucket.org/beatngu13/pdfbookmarkwizard/downloads/) the Wizard and launch the JAR-file by simply double-clicking on it. That's it!

If you are running in any trouble, feel free to open an [issue](https://bitbucket.org/beatngu13/pdfbookmarkwizard/issues/).

## Troubleshooting ##

This application uses JavaFX, in case your system runs with OpenJDK, you might consider having a look at [this](http://stackoverflow.com/questions/18547362/javafx-and-openjdk/).